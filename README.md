# Résumé
Projet Maven et Intégration Continue autour de la suite de Fibonacci


# Auteur
Alice Gonnaud

Février 2019

# Dépôt du code

Le code est déposé sur GitLab à l'adresse suivante :

https://gitlab.com/AliceG/gonnaud_maven_ci_fibonacci/

Le dépôt est public.

# Description du dépôt

Le code source est dans le répertoire _gonnaud_fibonacci_ à la racine. 

Il s'agit d'un projet Maven, qui en suit donc les conventions d'organisation.

Dans ce répertoire :

* Répertoire _appli_ : Module application Spring-Boot (dépend du module core)

* Répertoire _core_ : Implémentation de la suite de Fibonacci, et tests unitaires

* Répertoire _plugin_ : Plugin Maven (dépend du module core)

* _.gitlab-ci.yml_ : Pipeline pour l'intégration continue GitLab

Chaque module contient par ailleurs son _pom.xml_ .



# READMEs

Veuillez trouver à la racine :

* Ce *README* de présentation générale
* *README_Maven*, qui documente la partie du projet Maven
* *README_CI*, qui documente la partie du projet Intégration continue

<!-- Commentaire pour tester une MR -->
