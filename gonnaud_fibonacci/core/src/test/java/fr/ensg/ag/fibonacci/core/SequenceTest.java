package fr.ensg.ag.fibonacci.core;

import org.junit.Test;
import static org.junit.Assert.*;

public class SequenceTest {

    @Test
    public void compute_fibonacci() {
        assertEquals(0, Sequence.compute_fibonacci(0));
        assertEquals(1, Sequence.compute_fibonacci(1));
        assertEquals(0, Sequence.compute_fibonacci(-1));
        assertEquals(0, Sequence.compute_fibonacci(Integer.MIN_VALUE));
        assertEquals(233, Sequence.compute_fibonacci(13));
    }
}