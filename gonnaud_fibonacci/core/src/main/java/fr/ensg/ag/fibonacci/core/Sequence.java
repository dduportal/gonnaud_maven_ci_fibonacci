package fr.ensg.ag.fibonacci.core;

public class Sequence {

    public static int compute_fibonacci(int n) {

        if (n <= 0) {
            return 0;
        }

        else if (n == 1) {
            return 1;
        }

        else
            return compute_fibonacci(n - 1) + compute_fibonacci(n - 2);
    }

}
