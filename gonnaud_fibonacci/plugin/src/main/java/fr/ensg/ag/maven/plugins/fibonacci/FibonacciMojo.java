package fr.ensg.ag.maven.plugins.fibonacci;

import fr.ensg.ag.fibonacci.core.Sequence;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Parameter;

@org.apache.maven.plugins.annotations.Mojo(name="fibonacci")
public class FibonacciMojo extends AbstractMojo{

    @Parameter(property = "rank")
    private int rank_n;

    @Override
    public void execute(){

        System.out.println(Sequence.compute_fibonacci(rank_n));
    }
}